/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const ClientIdentity = require('fabric-shim').ClientIdentity;

class StudentRecord extends Contract {

    
    async initLedger(ctx) {
        console.info('============= START : Initialize Ledger ===========');
        const infos = [
            {
                id: 'roll001',
                //roll No is to unique for the students.
                rollno: 'roll001',
                name: 'student',
                mark: 0.00
            }
        ];

        for (let i = 0; i < infos.length; i++) {
            // infos[i].docType = 'car';
            await ctx.stub.putState(infos[i]['id'], Buffer.from(JSON.stringify(infos[i])));
            console.info('Added <--> ', infos[i]);
        }
        console.info('============= END : Initialize Ledger ===========');
    }

    async queryStudent(ctx, rollno) {
        if (rollno.length <= 0) {
            throw new Error(`Please provide rollno param!`);
        }
        let cid = new ClientIdentity(ctx.stub); // "stub" is the ChaincodeStub object passed to Init() and Invoke() methods
        var callerMspid = cid.getMSPID()
        console.info("callerMspid--MSPID--", callerMspid);
        var callerid = cid.getID()
        console.info("callerid", callerid)
        var callerCertificate = cid.getX509Certificate()
        console.info("callerCertificate", callerCertificate)
        const studentAsBytes = await ctx.stub.getState(rollno); // get the car from chaincode state
        if (!studentAsBytes || studentAsBytes.length === 0) {
            throw new Error(`${rollno} does not exist`);
        }
        console.info(studentAsBytes.toString());
        return studentAsBytes.toString();
    }

    async addStudentRecord(ctx, rollno, mark, name) {

        if (rollno.length <= 0) {
            throw new Error(`Please provide rollno param!`);
        }

        if (name.length <= 0) {
            throw new Error(`Please provide name param!`);
        }
        console.info('========1===== mark ===========', mark);
     
        if (! parseFloat(mark.match(/^-?\d*(\.\d+)?$/)) > 0) {
            throw new Error(`Please provide valid mark param!`);
        }

        if ( mark < 0) {
            throw new Error(`Please provide value of student mark!`);
        }
        
        console.info('============= START : addStudentRecord ===========', rollno, mark, name);
        const studentAsBytes = await ctx.stub.getState(rollno); // get the car from chaincode state
        console.info('============= studentAsBytes ===========', studentAsBytes);

        if (studentAsBytes && studentAsBytes.length > 0) {
            throw new Error(`${rollno} already exist`);
        }
        const student = {
            id: rollno,
            rollno,
            name,
            mark
        };

        await ctx.stub.putState(rollno, Buffer.from(JSON.stringify(student)));
        console.info('============= END : addStudentRecord ===========');
    }

    async queryAllStudents(ctx) {
        const startKey = '';
        const endKey = '';

        const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString('utf8'));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString('utf8');
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async updateStudentMark(ctx, rollno, newmark) {

        if (rollno.length <= 0) {
            throw new Error(`Please provide rollno param!`);
        }
        
        if (!parseFloat(newmark.match(/^-?\d*(\.\d+)?$/)) > 0) {
            throw new Error(`Please provide newmark param!`);
        }

        console.info('============= START : updateStudentHash ===========');

        const studentAsBytes = await ctx.stub.getState(rollno); // get the user from chaincode state
        if (!studentAsBytes || studentAsBytes.length === 0) {
            throw new Error(`${rollno} does not exist`);
        }
        var student = JSON.parse(studentAsBytes.toString());
        console.info('========previous===== student : ===========', student);

        student.mark = newmark;
        console.info('========new info to be updated===== student : ===========', student);

        await ctx.stub.putState(rollno, Buffer.from(JSON.stringify(student)));
        console.info('============= END : updateStudentHash ===========');
    }
    async getHistoryForUserStudent(ctx, rollno) {
        if (rollno.length <= 0) {
            throw new Error(`Please provide rollno param!`);
        }
        console.info('============= START : getHistoryForUserStudent ===========', rollno);
        if (rollno.length <= 0) {
            throw new Error('rollno is required to fetch the  history');
        }
        let iterator = await ctx.stub.getHistoryForKey(rollno);

        let allResults = [];
        while (true) {
            let res = await iterator.next();
            console.info('============= KEY___START========');

            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                console.info(res.value.value.toString('utf8'));
                console.info('---value--', res.value.value.toString('utf8'));
                console.info('---res.value--', res.value);
                let txId = res.value.tx_id;
                let timestamp = res.value.timestamp;
                let Key = res.value.key;
                console.info('---Key--', Key);
                jsonRes.txId = txId
                jsonRes.timestamp = timestamp
                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                }
                catch (err) {
                    console.info('----ERROR---', err);
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
                console.info('============= KEY___END========');

            }
            if (res.done) {
                console.info('============= END : getHistoryForUserStudent ===========');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }

    }

}

module.exports = StudentRecord;
